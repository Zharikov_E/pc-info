/* Options config */
const ROUTES_BY_OPTION = [{
        option: 'cpu',
        api: ['cpu'],
        handler: (cpu) => {
            drawProperties([
                ['Brand', cpu.brand],
                ['Vendor', cpu.manufacturer],
                ['Manufacturer', cpu.manufacturer],
                ['Cores', cpu.cores],
                ['Physical Cores', cpu.physicalCores],
                ['Processors', cpu.processors],
                ['Family', cpu.family],
                ['Model', cpu.model],
                ['Revision', cpu.revision],
                ['Socket', cpu.socket],
                ['Frequency', `${cpu.speed} GHz`],
                ['Speed Max', cpu.speedmax],
                ['Speed Min', cpu.speedmin],
                ['Stepping', cpu.stepping],
                ['Voltage', cpu.voltage],
            ])
        }
    },
    {
        option: 'motherboard',
        api: ['baseboard'],
        handler: (baseboard) => {
            drawProperties([
                ['manufacturer', baseboard.manufacturer],
                ['model', baseboard.model],
                ['serial', baseboard.serial],
                ['version', baseboard.version],
            ])
        }
    },
    {
        option: 'bios',
        api: ['bios'],
        handler: (bios) => {
            drawProperties([
                ['vendor', bios.vendor],
                ['version', bios.version],
                ['Release Date', bios.releaseDate],
                ['revision', bios.revision],
            ])
        }
    },
    {
        option: 'keyboardKeysDown',
        api: ['keyboardKeydown'],
        handler: (keyboardKeysInfo) => {

            drawHeader('General Info:');

            drawProperties([
                ['Keys pressed', keyboardKeysInfo.length],
            ]);

            const keys = keyboardKeysInfo.reduce((keysSet, next) => {
                if (keysSet[next.rawcode] !== undefined) {
                    keysSet[next.rawcode].count++;
                } else {
                    keysSet[next.rawcode] = {
                        key: next,
                        count: 1
                    };
                }
                return keysSet;
            }, {});


            drawHeader('Keys usage:');
            drawProperties(
                Object.entries(keys).map(([, keyInfo]) => {
                    return [`Key code ${keyInfo.key.rawcode} (${keyInfo.key.char})`, `${keyInfo.count}`];
                })
            );
        }
    },
    {
        option: 'blockDevices',
        api: ['blockDevices'],
        handler: (disksInfo) => {
            disksInfo.forEach(disk => {
                drawSubheader('Disk ' + disk.identifier);
                drawProperties([
                    ['identifier', disk.identifier],
                    ['label', disk.label],
                    ['model', disk.model],
                    ['mount', disk.mount],
                    ['name', disk.name],
                    ['physical', disk.physical],
                    ['protocol', disk.protocol],
                    ['removable', disk.removable],
                    ['serial', disk.serial],
                    ['size', disk.size],
                    ['type', disk.type],
                    ['uuid', disk.uuid],
                ])
            })

        }
    },
    {
        option: 'chassis',
        api: ['chassis'],
        handler: (chassis) => {
            drawProperties([
                ['manufacturer', chassis.manufacturer],
                ['model', chassis.model],
                ['serial', chassis.serial],
                ['sku', chassis.sku],
                ['type', chassis.type],
                ['version', chassis.version],
            ])
        }
    },
    {
        option: 'cpuCache',
        api: ['cpuCache'],
        handler: (cpuCache) => {}
    },
    {
        option: 'cpuCurrentSpeed',
        api: ['cpuCurrentSpeed'],
        handler: (cpuCurrentSpeed) => {}
    },
    {
        option: 'cpuFlags',
        api: ['cpuFlags'],
        handler: (cpuFlags) => {}
    },
    {
        option: 'cpuTemperature',
        api: ['cpuTemperature'],
        handler: (cpuTemperature) => {}
    },
    {
        option: 'currentLoad',
        api: ['currentLoad'],
        handler: (currentLoad) => {}
    },
    {
        option: 'diskLayout',
        api: ['diskLayout'],
        handler: (diskLayout) => {
            diskLayout.forEach(disk => {
                drawSubheader(disk.name);
                drawProperties([
                    ['bytes Per Sector', disk.bytesPerSector],
                    ['device', disk.device],
                    ['firmware Revision', disk.firmwareRevision],
                    ['interface Type', disk.interfaceType],
                    ['name', disk.name],
                    ['sectors Per Track', disk.sectorsPerTrack],
                    ['serial Num', disk.serialNum],
                    ['size', disk.size],
                    ['smart Status', disk.smartStatus],
                    ['total Cylinders', disk.totalCylinders],
                    ['total Heads', disk.totalHeads],
                    ['total Sectors', disk.totalSectors],
                    ['total Tracks', disk.totalTracks],
                    ['tracks Per Cylinder', disk.tracksPerCylinder],
                    ['type', disk.type],
                    ['vendor', disk.vendor],
                ])
            })
        }
    },
    {
        option: 'fsSize',
        api: ['fsSize'],
        handler: (fsSize) => {

            fsSize.forEach(disk => {
                drawSubheader(`Drive ${disk.fs}`);
                drawProperties([
                    ['File System', disk.fs],
                    ['Mount', disk.mount],
                    ['size', disk.size],
                    ['type', disk.type],
                    ['Use', disk.use],
                    ['used', disk.used],
                ])
            })
        }
    },
    {
        option: 'graphics',
        api: ['graphics'],
        handler: (graphics) => {
            drawHeader('Video Controllers');
            graphics.controllers.forEach(videoCard => {
                drawSubheader(`Controller ${videoCard.model}`);

                drawProperties([
                    ['bus', videoCard.bus],
                    ['model', videoCard.model],
                    ['vendor', videoCard.vendor],
                    ['vram', videoCard.vram],
                    ['vram Dynamic', videoCard.vramDynamic],
                ])
            });

            drawHeader('Displays');
            graphics.displays.forEach(display => {
                drawSubheader(`Display ${display.model}`);

                drawProperties([
                    ['builtin', display.builtin],
                    ['connection', display.connection],
                    ['current Refresh Rate', display.currentRefreshRate],
                    ['current Res X', display.currentResX],
                    ['current Res Y', display.currentResY],
                    ['main', display.main],
                    ['model', display.model],
                    ['pixeldepth', display.pixeldepth],
                    ['positionX', display.positionX],
                    ['positionY', display.positionY],
                    ['resolution x', display.resolutionx],
                    ['resolution y', display.resolutiony],
                    ['size x', display.sizex],
                    ['size y', display.sizey],
                    ['vendor', display.vendor],
                ])
            });
        }
    },
    {
        option: 'memory',
        api: ['memory'],
        handler: (memory) => {
            drawProperties([
                ['active', memory.active],
                ['available', memory.available],
                ['buffcache', memory.buffcache],
                ['buffers', memory.buffers],
                ['cached', memory.cached],
                ['free', memory.free],
                ['slab', memory.slab],
                ['swapfree', memory.swapfree],
                ['swaptotal', memory.swaptotal],
                ['swapused', memory.swapused],
                ['total', memory.total],
                ['used', memory.used],
            ])
        }
    },
    {
        option: 'memoryLayout',
        api: ['memoryLayout'],
        handler: (memoryLayout) => {
            memoryLayout.forEach(ramUnit => {
                drawHeader(`${ramUnit.bank} ${ramUnit.formFactor}`);
                drawProperties([
                    ['bank', ramUnit.bank],
                    ['clockSpeed', ramUnit.clockSpeed],
                    ['formFactor', ramUnit.formFactor],
                    ['manufacturer', ramUnit.manufacturer],
                    ['partNum', ramUnit.partNum],
                    ['serialNum', ramUnit.serialNum],
                    ['size', ramUnit.size],
                    ['type', ramUnit.type],
                    ['voltageConfigured', ramUnit.voltageConfigured],
                    ['voltageMax', ramUnit.voltageMax],
                    ['voltageMin', ramUnit.voltageMin],
                ]);
            })
        }
    },
    {
        option: 'networkConnections',
        api: ['networkConnections'],
        handler: (networkConnections) => {
            drawTable([
                    ['Protocol', 'protocol'],
                    ['Loc. Address', 'localaddress'],
                    ['Loc. Port', 'localport'],
                    ['Process ID', 'pid'],
                    ['State', 'state'],
                ],
                networkConnections
            );
        }
    },
    {
        option: 'networkGatewayDefault',
        api: ['networkGatewayDefault'],
        handler: (networkGatewayDefault) => {
            drawProperties([
                ['network Gateway Default', networkGatewayDefault],
            ])
        }
    },
    {
        option: 'networkInterfaceDefault',
        api: ['networkInterfaceDefault'],
        handler: (networkInterfaceDefault) => {
            drawProperties([
                ['network Interface Default', networkInterfaceDefault],
            ])
        }
    },
    {
        option: 'networkInterfaces',
        api: ['networkInterfaces'],
        handler: (networkInterfaces) => {
            networkInterfaces.forEach(interface => {
                drawSubheader(`${interface.ifaceName}:`);
                drawProperties([
                    ['ifaceName', interface.ifaceName],
                    ['Interface', interface.iface],
                    ['DHCP', interface.dhcp],
                    ['duplex', interface.duplex],
                    ['internal', interface.internal],
                    ['ip4', interface.ip4],
                    ['ip4subnet', interface.ip4subnet],
                    ['ip6', interface.ip6],
                    ['ip6subnet', interface.ip6subnet],
                    ['mac', interface.mac],
                    ['speed', interface.speed],
                    ['type', interface.type],
                    ['virtual', interface.virtual],
                ])
            });
        }
    },
    {
        option: 'networkStats',
        api: ['networkStats'],
        handler: (networkStats) => {}
    },
    {
        option: 'osInfo',
        api: ['osInfo'],
        handler: (osInfo) => {
            drawProperties([
                ['arch', osInfo.arch],
                ['build', osInfo.build],
                ['codename', osInfo.codename],
                ['codepage', osInfo.codepage],
                ['distro', osInfo.distro],
                ['hostname', osInfo.hostname],
                ['kernel', osInfo.kernel],
                ['logofile', osInfo.logofile],
                ['platform', osInfo.platform],
                ['release', osInfo.release],
                ['serial', osInfo.serial],
                ['servicepack', osInfo.servicepack],
                ['uefi', osInfo.uefi],
            ])
        }
    },
    {
        option: 'processes',
        api: ['processes'],
        handler: (processes) => {
            drawTable(
                [
                    ['Process ID', 'pid'],
                    ['Parent PID', 'parentPid'],
                    ['Name', 'name'],
                    ['PCPU', 'pcpu'],
                    ['priority', 'priority'],
                    ['started', 'started'],
                    ['state', 'state'],
                ],
                processes.list,
            );
        }
    },
    {
        option: 'system',
        api: ['system'],
        handler: (system) => {
            drawProperties([
                ['manufacturer', system.manufacturer],
                ['model', system.model],
                ['serial', system.serial],
                ['sku', system.sku],
                ['uuid', system.uuid],
                ['version', system.version],
            ])
        }
    },
    {
        option: 'time',
        api: ['time'],
        handler: (time) => {
            drawProperties([
                ['Current', new Date(time.current)],
                ['Timezone', time.timezone],
                ['Timezone Name', time.timezoneName],
                ['Uptime', `${time.uptime} seconds`],
            ]);
        }
    },
    {
        option: 'users',
        api: ['users'],
        handler: (users) => {
            drawTable([
                ['User', 'user']
            ], users);
        }
    },
    {
        option: 'uuid',
        api: ['uuid'],
        handler: (uuid) => {
            drawProperties([
                ['OS Uuid', uuid.os],
            ])
        }
    },
    {
        option: 'wifiNetworks',
        api: ['wifiNetworks'],
        handler: (wifiNetworks) => {}
    },
    {
        option: 'mouseclicks',
        api: ['mouseclicks'],
        handler: (mouseclicks) => {
            const clicks = {
                1: 0,
                2: 0,
                3: 0,
            };
            mouseclicks.forEach(click => {
                clicks[click.button]++;
            });
            drawProperties([
                ['Total clicks', mouseclicks.length],
                ['Left button clicks', clicks[1]],
                ['Right button clicks', clicks[2]],
                ['Wheel clicks', clicks[3]],
            ]);
        }
    },
];




/* Keep currently selected option for refreshing. */
let currentOption;

document.getElementById('infoSelect').addEventListener('change', function(event) {
    const select = event.target;
    const optionText = select.options[select.selectedIndex].text;
    const optionValue = select.options[select.selectedIndex].value;

    const routeOption = ROUTES_BY_OPTION.find(config => config.option === optionValue);
    currentOption = routeOption;
    loadInfoByOption(routeOption);

});

function loadInfoByOption(routeOption) {

    const apiCalls = routeOption.api.map(apiPath => {
        return getPcInfo(apiPath);
    });

    showLoader();
    return Promise.all(apiCalls).then(results => {
        hideLoader();
        cleanInfoBlock();
        routeOption.handler(...results);
        refreshButton.classList.remove('hidden');
    });
}

/* Refresh */

const refreshButton = document.getElementById('refresh');

refreshButton.addEventListener('click', function() {
    loadInfoByOption(currentOption);
});

/* Loader */

const loader = document.getElementById('loading');
const backdrop = document.getElementById('backdrop');

function showLoader() {
    loader.classList.remove('hidden');
    backdrop.classList.remove('hidden');
}

function hideLoader() {
    loader.classList.add('hidden');
    backdrop.classList.add('hidden');
}

/* Info container */

const infoBlock = document.getElementById('info');

function cleanInfoBlock() {
    infoBlock.innerHTML = '';
}

async function getPcInfo(path) {
    return fetch(`api/info/${path}`).then(resp => resp.json());
}

async function collectApiData(paths) {
    showLoader();
    const apiCalls = paths.map(path => {
        return getPcInfo(path);
    });

    return Promise.all(apiCalls).then(function(responses) {
        hideLoader();
        const mappedResponses = responses.map((response, index) => {
            return {
                data: response,
                apiPath: paths[index],
            };
        });
        const collectedData = mappedResponses.reduce((collection, next) => {
            collection[next.apiPath] = next.data;
            return collection;
        }, {});
        return collectedData;
    });
}

/* Render Info */

function drawProperties(properties) {
    properties.forEach(property => {
        const propValue = typeof property[1] !== undefined && String(property[1]).trim() ? property[1] : 'Not specified';
        infoBlock.insertAdjacentHTML(
            "beforeend",
            `<div class="property">
                <div>${property[0]}:</div>
                <div>${propValue}</div>
            </div>`
        )
    });

}

function drawHeader(text) {
    infoBlock.insertAdjacentHTML(
        `beforeend`,
        `<div class='header'>${text}</div>`
    );
}

function drawSubheader(text) {
    infoBlock.insertAdjacentHTML(
        `beforeend`,
        `<div class='subheader'>${text}</div>`
    );
}

function drawTable(headers, data) {
    const headersHtml = headers.map(header => {
        return `<div>${header[0]}</div>`;
    });
    const rows = data.map(object => {
        const cells = headers.map(header => {
            return `<div title='${object[header[1]]}'>${object[header[1]]}</div>`;
        });
        return `<div class='table-row'>${cells.join('')}</div>`;
    });
    infoBlock.insertAdjacentHTML(
        `beforeend`,
        `<div class='table'>
            <div class='table-header'>
                ${headersHtml.join('')}
            </div>
            ${rows.join('')}
         </div>
        `
    )
}

/* Download */

const downloadButton = document.getElementById('download');

downloadButton.addEventListener('click', async function() {
    const allApis = [
        `baseboard`,
        `battery`,
        `bios`,
        `blockDevices`,
        `chassis`,
        `cpu`,
        `cpuCache`,
        `cpuCurrentSpeed`,
        `cpuFlags`,
        `cpuTemperature`,
        `currentLoad`,
        `diskLayout`,
        `graphics`,
        `memory`,
        `memoryLayout`,
        `networkConnections`,
        `networkGatewayDefault`,
        `networkInterfaceDefault`,
        `networkInterfaces`,
        `networkStats`,
        `osInfo`,
        `fsOpenFiles`,
        `fsSize`,
        `processes`,
        `services`,
        `system`,
        `time`,
        `users`,
        `uuid`,
        `version`,
        `versions`,
        `wifiNetworks`,
    ];
    const data = await collectApiData(allApis);
    console.log(data);
    const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
    const anchor = document.createElement('a');

    anchor.download = "pc-info.json";
    anchor.href = (window.webkitURL || window.URL).createObjectURL(blob);
    anchor.dataset.downloadurl = ['application/json', anchor.download, anchor.href].join(':');
    anchor.click();
});