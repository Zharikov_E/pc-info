const express = require('express');
const app = express();
const ioHook = require('iohook');

const si = require('systeminformation');

const PORT = 3000;

app.use(express.static('static'));

const PC_INFO_ROOT_PATH = '/api/info';


const MOUSE_CLICKS = [];
const KEY_PRESSES = [];

ioHook.addListener('mouseclick', function(mouseClickEvent) {
    MOUSE_CLICKS.push(mouseClickEvent);
});

ioHook.addListener('keydown', function(keyDownEvent) {
    KEY_PRESSES.push({
        ...keyDownEvent,
        char: String.fromCharCode(keyDownEvent.rawcode)
    });
})

ioHook.start();

const PC_INFO_ROUTES_CONFIG = [
    { path: `baseboard`, jsonHandler: si.baseboard },
    { path: `battery`, jsonHandler: si.battery },
    { path: `bios`, jsonHandler: si.bios },
    { path: `blockDevices`, jsonHandler: si.blockDevices },
    { path: `chassis`, jsonHandler: si.chassis },
    { path: `cpu`, jsonHandler: si.cpu },
    { path: `cpuCache`, jsonHandler: si.cpuCache },
    { path: `cpuCurrentSpeed`, jsonHandler: si.cpuCurrentspeed },
    { path: `cpuFlags`, jsonHandler: si.cpuFlags },
    { path: `cpuTemperature`, jsonHandler: si.cpuTemperature },
    { path: `currentLoad`, jsonHandler: si.currentLoad },
    { path: `diskLayout`, jsonHandler: si.diskLayout },
    // { path: `disksIO`, jsonHandler: si.disksIO },
    { path: `fsOpenFiles`, jsonHandler: si.fsOpenFiles },
    { path: `fsSize`, jsonHandler: si.fsSize },
    // { path: `fsStats`, jsonHandler: si.fsStats },
    // { path: `fullLoad`, jsonHandler: si.fullLoad },
    { path: `graphics`, jsonHandler: si.graphics },
    { path: `memory`, jsonHandler: si.mem },
    { path: `memoryLayout`, jsonHandler: si.memLayout },
    { path: `networkConnections`, jsonHandler: si.networkConnections },
    { path: `networkGatewayDefault`, jsonHandler: si.networkGatewayDefault },
    { path: `networkInterfaceDefault`, jsonHandler: si.networkInterfaceDefault },
    { path: `networkInterfaces`, jsonHandler: si.networkInterfaces },
    { path: `networkStats`, jsonHandler: si.networkStats },
    { path: `osInfo`, jsonHandler: si.osInfo },
    // { path: `processLoad`, jsonHandler: si.processLoad },
    { path: `processes`, jsonHandler: si.processes },
    { path: `services`, jsonHandler: si.services },
    // { path: `shell`, jsonHandler: si.shell },
    { path: `system`, jsonHandler: si.system },
    { path: `time`, jsonHandler: si.time },
    { path: `users`, jsonHandler: si.users },
    { path: `uuid`, jsonHandler: si.uuid },
    { path: `version`, jsonHandler: si.version },
    { path: `versions`, jsonHandler: si.versions },
    { path: `wifiNetworks`, jsonHandler: si.wifiNetworks },
    {
        path: `mouseclicks`,
        jsonHandler: function() {
            return Promise.resolve(MOUSE_CLICKS);
        }
    },
    {
        path: `keyboardKeydown`,
        jsonHandler: function() {
            return Promise.resolve(KEY_PRESSES);
        }
    },

    /* Interesting ones */
    // { path: `getAllData`, jsonHandler: si.getAllData },
];

PC_INFO_ROUTES_CONFIG.forEach(config => {
    const apiPath = `${PC_INFO_ROOT_PATH}/${config.path}`;
    app.get(apiPath, function(req, resp) {
        console.log(`request on ${apiPath}`);
        const handlerResult = config.jsonHandler();
        if (handlerResult instanceof Promise) {
            handlerResult.then(info => {
                resp.json(info);
            });
        } else if (handlerResult instanceof Function) {
            const nestedFunctionResult = handlerResult();
            if (nestedFunctionResult instanceof Promise) {
                nestedFunctionResult.then(info => {
                    resp.json(info);
                });
            } else {
                resp.json(infoBlock);
            }
        } else {
            resp.json(handlerResult);
        }
    });
});

app.listen(PORT, () => {
    console.log('App started on port ', PORT);
});