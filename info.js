const si = require('systeminformation');
const ioHook = require('iohook');
/* 
ioHook.on("mousemove", event => {
    console.log('mouse click', event);
    /* You get object like this
    {
       type: 'mousemove',
       x: 700,
       y: 400
     }
});
// For keyboard hook
ioHook.on("keydown", event => { 
    console.log('keydown', event);
});
ioHook.on("keyup", event => { 
    console.log('keyup', event);
});

//Register and start hook 
ioHook.start();
 */
// si.dockerAll().then(console.log);
// si.dockerContainerProcesses().then(console.log);
// si.dockerContainerProcesses().then(console.log);
function log(name) {
    return function (arg) {
        console.log(name, ': ', arg);
    }
}

si.graphics().then((graphics) => {
    console.log(graphics.controllers);
    console.log(graphics.displays);
});


/**
si.system().then(log('system'));
console.log('time: ', si.time());
si.users().then(log('users'));
si.uuid().then(log('uuid'));
console.log('version: ', si.version());
si.versions().then(log('versions'));
si.cpu().then(log('cpu'));
si.baseboard().then(log('baseboard'));
si.battery().then(log('battery'));
si.bios().then(log('bios'));
si.blockDevices().then(log('block devices'));
si.chassis().then(log('chassis'));
si.cpuCache().then(log('cpu cache'));
si.cpuCurrentspeed().then(log('cpu current speed'));
si.cpuFlags().then(log('cpu flags'));
si.cpuTemperature().then(log('cpu temperature'));
si.currentLoad().then(log('current load'));
si.diskLayout().then(log('disk layout'));
si.disksIO().then(log('disks io'));
si.fsOpenFiles().then(log('fs open files'));
si.fsSize().then(log('fs size'));
si.fsStats().then(log('fs stats'));
si.fullLoad().then(log('full load'));
si.getAllData().then(log('get all data'));

// si.inetChecksite().then(console.log);
si.inetLatency().then(log('inet latency'));
si.mem().then(log('mem'));
si.memLayout().then(log('mem layout'));
si.networkConnections().then(log('network connections'));
si.networkGatewayDefault().then(log('network gateway default'));
si.osInfo().then(log('os info'));
// si.processLoad().then(log('process load'));
si.processes().then(log('processes'));
si.services().then(log('services'));
si.shell().then(log('shell'));
si.system().then(log('system'));
console.log('time: ', si.time());
si.users().then(log('users'));
si.uuid().then(log('uuid'));
console.log('version: ', si.version());
si.versions().then(log('versions'));

si.wifiNetworks().then(log('wifi networks'));
si.networkInterfaceDefault().then(console.log);
si.networkInterfaces().then(console.log);
si.networkStats().then(console.log);


*/